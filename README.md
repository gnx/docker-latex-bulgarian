# docker-latex-bulgarian
A docker image I created for my Bulgarian publications. Based on Ubuntu.
Currently uses texlive-full and adds some nice Bulgarian cyrillic fonts, but I intend to make it as slim as possible for my specific usecase to make my LaTeX build pipeline as fast as possible.
That is why I would not recommend using it as an image, but it could be a good starting point to create your own dockerfile.
